#
# Copyright (c) 2013 Aurélien Aptel <aurelien.aptel@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#

use strict;
use warnings;
use PCP::PMDA;
use Data::Dumper;
use constant {
    CLUSTER_GLOB  => 0,
    CLUSTER_MOUNT => 1,
    CLUSTER_TOTAL => 2,

    ALL_MOUNT_INDOM  => 0,
    SMB1_MOUNT_INDOM => 1,
    SMB2_MOUNT_INDOM => 2,

    DEFAULT_STAT_PATH => '/proc/fs/cifs/Stats',
    PMID => 121,
    DOM => 'cifs',
    DOM_SMB1 => 'smb1',
    DOM_SMB2 => 'smb2',
};

my $PROCFILE = $ENV{CIFS_STAT_PATH} || DEFAULT_STAT_PATH;

# PCP::PMDA instance
my $PMDA;

# local $SIG{__WARN__} = sub {
#     $PMDA->log(shift());
# };

# Maps share name to (instance number, smb version)
my %SHARES;

# Maps (smb version, item name) to item number
my %ITEMS;

# Maps pmid number to their indom
my %PMID_INDOM;

# Data sent at each fetch_cb()
# accessed using:
#    $RAW[cluster][item][inst]
# or $RAW[cluster][item]
# depending on the cluster
my @RAW;

#
# Exposed metrics
#

my %METRICS = (

    'all' => {

        # global metrics (permount = 0)
        #  - cluster 0
        #  - no total
        #  - regex matched only in the header part of the file

        'global' => {
            'num_session' => {
                'doc' => q{Number of session active on the machine},
                'regex' => qr{^CIFS Session: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_operation' => {
                'doc' => q{Number of operation executed on the machine},
                'regex' => qr{Total vfs operations: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem'  => PM_SEM_COUNTER,
            },
        },

        # per-mount metrics (permount = 1)
        #  - cluster 1
        #  - expose total in cifs.total.xxx @ cluster 2
        #  - regex matched only in the per-mount part of the file

        'permount' => {
            'num_smbs' => {
                'regex' => qr{^SMBs: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
        },
    },

    'smb1' => {
        'permount' => {
            'num_read' => {
                'doc' => 'Number of calls to Read()',
                'regex' => qr{Reads:\s+(\d+)\s+Bytes:\s+(\d+)},
                'code' => sub {
                    {'num_read' => $1, 'byte_read' => $2}
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'byte_read' => {
                # XXX: set with num_read
                'doc' => q{Number of bytes read},
                'unit' => pmda_units(1,0,0, PM_SPACE_BYTE,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_write' => {
                'doc' => 'Number of calls to Write()',
                'regex' => qr{Writes:\s+(\d+)\s+Bytes:\s+(\d+)},
                'code' => sub {
                    {'num_write' => $1, 'byte_write' => $2}
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'byte_write' => {
                # XXX: set with num_write
                'doc' => 'Number of bytes written',
                'unit' => pmda_units(1,0,0, PM_SPACE_BYTE,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_flush' => {
                # TODO: doc
                'regex' => qr{^Flushes:\s+(\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_lock' => {
                # TODO: doc
                'regex' => qr{^Locks:\s+(\d+)\s+HardLinks:\s+(\d+)\s+Symlinks:\s+(\d+)},
                'code' => sub {
                    {'num_lock' => $1, 'num_hardlink' => $2, 'num_symlink' => $3}
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_hardlink' => {
                # XXX: set with num_lock
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_symlink' => {
                # XXX: set with num_lock
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_open' => {
                # TODO: doc
                'regex' => qr{^Opens:\s+(\d+)\s+Closes:\s+(\d+)\s+Deletes:\s+(\d+)},
                'code' => sub {
                    {'num_open' => $1, 'num_close' => $2, 'num_delete' => $3}
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_close' => {
                # XXX: set with num_open
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_delete' => {
                # XXX: set with num_open
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_posix_open' => {
                # TODO: doc
                'regex' => qr{^Posix Opens:\s+(\d+)\s+Posix Mkdirs:\s+(\d+)},
                'code' => sub {
                    {'num_posix_open' => $1, 'num_posix_mkdir' => $2}
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_posix_mkdir' => {
                # XXX: set with num_posix_open
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_mkdir' => {
                # TODO: doc
                'regex' => qr{^Mkdirs:\s+(\d+)\s+Rmdirs:\s+(\d+)},
                'code' => sub {
                    {'num_mkdir' => $1, 'num_rmdir' => $2},
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_rmdir' => {
                # XXX: set with num_mkdir
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_rename' => {
                # TODO: doc
                'regex' => qr{^Renames:\s+(\d+)\s+T2 Renames\s+(\d+)},
                'code' => sub {
                    {'num_rename' => $1, 'num_t2rename' => $2},
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_t2rename' => {
                # XXX: set with num_rename
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },

            'num_findfirst' => {
                # TODO: doc
                'regex' => qr{^FindFirst:\s+(\d+)\s+FNext\s+(\d+)\s+FClose\s+(\d+)},
                'code' => sub {
                    {'num_findfirst' => $1, 'num_fnext' => $2, 'num_fclose' => $3}
                },
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_fnext' => {
                # XXX: set with num_lock
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
            'num_fclose' => {
                # XXX: set with num_lock
                # TODO: doc
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_U32,
                'sem'  => PM_SEM_COUNTER,
            },
        },
    },
    'smb2' => {
        'permount' => {
            'num_negotiate' => {
                'regex' => qr{^Negotiates: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_session_setup' => {
                'regex' => qr{^SessionSetups: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_logoff' => {
                'regex' => qr{^Logoffs: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_tree_connect' => {
                'regex' => qr{^TreeConnects: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_tree_disconnect' => {
                'regex' => qr{^TreeDisconnects: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_create' => {
                'regex' => qr{^Creates: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_close' => {
                'regex' => qr{^Closes: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_flush' => {
                'regex' => qr{^Flushes: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_read' => {
                'regex' => qr{^Reads: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_write' => {
                'regex' => qr{^Writes: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_lock' => {
                'regex' => qr{^Locks: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_ioctl' => {
                'regex' => qr{^IOCTLs: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_cancel' => {
                'regex' => qr{^Cancels: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_echo' => {
                'regex' => qr{^Echos: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_query_directory' => {
                'regex' => qr{^QueryDirectories: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_change_notify' => {
                'regex' => qr{^ChangeNotifies: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_query_info' => {
                'regex' => qr{^QueryInfos: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_set_info' => {
                'regex' => qr{^SetInfos: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
            'num_oplock_break' => {
                'regex' => qr{^OplockBreaks: (\d+)},
                'unit' => pmda_units(0,0,0,0,0,0),
                'type' => PM_TYPE_32,
                'sem' => PM_SEM_COUNTER,
            },
        },
    },
);

cifs_init();
$PMDA->run;

####

sub set_metric {
    my ($proto, $name, $value, $id) = @_;

    if (defined $id) {
        # if there's an id, we're in a per-mount case
        $RAW[CLUSTER_MOUNT][$ITEMS{$proto}{$name}][$id] = $value;
        $RAW[CLUSTER_TOTAL][$ITEMS{$proto}{$name.'_total'}][$id] += $value;
    } else {
        $RAW[CLUSTER_GLOB][$ITEMS{$proto}{$name}] = $value;
    }
}

sub match_and_set_metric {
    my ($proto, $name, $m, $line, $id) = @_;

    if ($m->{regex} && $line =~ $m->{regex}) {
        # if the metric has a regex and it matches

        if ($m->{code}) {
            # if the metric has a code, run it and use return value to set $RAW
            my $res = $m->{code}->();

            if (ref($res) eq 'HASH') {
                # if the return value is a hash, multiple metrics to set
                my ($k, $v);
                while (($k, $v) = each %$res) {
                    # use each key/value as metric in the same proto
                    set_metric($proto, $k, $v, $id);
                }
            } else {
                # if the return value is not a hash, use it as the
                # value (of the metric the code was defined for)
                set_metric($proto, $name, $res, $id);
            }
        } else {
            # if the metric doesn't have a code, use first capture
            # group of the regex as value
            set_metric($proto, $name, $1, $id);
        }
        $PMDA->log("match on $name (".$ITEMS{$proto}{$name}.") ".$m->{regex});
        return 1;
    }

    return 0;
}

sub parse_stat_file {
    open my $h, '<', $PROCFILE or die "Can't open $PROCFILE";

    my $inmount = 0;
    my $id;
    my $share;
    my $nbshare = 0;

    # reset totals
    for (@{$RAW[CLUSTER_TOTAL]}) {
        $_ = 0;
    }

    LINE: while (my $line = <$h>) {
        chomp $line;

        if ($line =~ /^\d+\) (.+)/) {
            # matched a share first line

            # set current share id
            if (exists $SHARES{$1}) {
                $id = $SHARES{$1}{id};
            } else {
                $id = keys %SHARES;
                $SHARES{$1}{id} = $id;
            }
            $inmount = 1;
            $share = $1;
            $nbshare++;
            next LINE;
        }

        if (!$inmount) {

            # Header section of the stats file, contains global stats

            # check protocol-neutral global stats
            # skip _protocol-specific_ global stats (doesn't exist)

            for my $name (keys %{$METRICS{all}{global}}) {
                my $m = $METRICS{all}{global}{$name};
                if (match_and_set_metric('all', $name, $m, $line)) {
                    next LINE;
                }
            }
        }

        else {

            # Mount section of the stats file

            # use cached proto if possible
            my @protos;

            if (defined $SHARES{$share}{proto}) {
                @protos = ($SHARES{$share}{proto});
            }
            else {
                @protos = ('all', 'smb1', 'smb2');
            }

            for my $proto (@protos) {
                for my $name (keys %{$METRICS{$proto}{permount}}) {
                    my $m = $METRICS{$proto}{permount}{$name};
                    if (match_and_set_metric($proto, $name, $m, $line)) {
                        if ($proto ne 'all') {
                            # if we have a match on a protocol-specific metric, cache it
                            $SHARES{$share}{proto} = $proto;
                        }
                        next LINE;
                    }
                }
            }
        }
    }

    if ($nbshare == 0) {
        $PMDA->log("no share mounted");
    }
    # print Dumper(\@RAW);
}

sub cifs_init {

    $PMDA = PCP::PMDA->new(DOM, PMID);
    my %indoms = ('all' => ALL_MOUNT_INDOM,
                  'smb1' => SMB1_MOUNT_INDOM,
                  'smb2' => SMB2_MOUNT_INDOM);

    my ($name, $m);
    my $i = 0;

    #
    # GLOBAL STATS (cluster 0)
    #

    # we can reset item number at each cluster
    $i = 0;

    # protocol-neutral global stats

    while (($name, $m) = each(%{$METRICS{all}{global}})) {
        my $item = $i++;
        my $pmid = pmda_pmid(CLUSTER_GLOB, $item);
        $ITEMS{all}{$name} = $item;
        $PMID_INDOM{$pmid} = PM_INDOM_NULL;

        $PMDA->add_metric($pmid,
                          $m->{type}, PM_INDOM_NULL, $m->{sem},
                          $m->{unit}, DOM.".$name",
                          $m->{doc} || '',
                          $m->{longdoc} || '');

    }

    # ignore protocol-specific global stats
    # (doesn't exists)

    #
    # PER MOUNT STATS (cluster 1)
    #

    $i = 0;

    for my $proto ('all', 'smb1', 'smb2') {
        my $in = $indoms{$proto};

        while (($name, $m) = each(%{$METRICS{$proto}{permount}})) {
            my $item = $i++;
            my $pmid = pmda_pmid(CLUSTER_MOUNT, $item);

            $ITEMS{$proto}{$name} = $item;
            $PMID_INDOM{$pmid} = $in;
            $PMDA->add_metric($pmid,
                              $m->{type}, $in, $m->{sem},
                              $m->{unit}, DOM.".$proto.$name",
                              $m->{doc} || '',
                              $m->{longdoc} || '');
        }
    }


    #
    # TOTAL STATS (cluster 2)
    #

    $i = 0;

    # for each per-mount stats, make a total one with no INDOM

    for my $proto ('all', 'smb1', 'smb2') {
        while (($name, $m) = each(%{$METRICS{$proto}{permount}})) {
            my $item = $i++;
            my $pmid = pmda_pmid(CLUSTER_TOTAL, $item);

            $ITEMS{$proto}{$name.'_total'} = $item;
            $PMID_INDOM{$pmid} = PM_INDOM_NULL;
            $PMDA->add_metric($pmid,
                              $m->{type}, PM_INDOM_NULL, $m->{sem},
                              $m->{unit}, DOM.".$proto.total.$name",
                              $m->{doc} || '',
                              $m->{longdoc} || '');

            # create initial value
            $RAW[CLUSTER_TOTAL][$item] = 0;
        }
    }



    # $PMDA->log(Dumper(\%ITEMS));
    parse_stat_file();

    # Add one INDOM per protocol
    $PMDA->add_indom(SMB1_MOUNT_INDOM, {}, '', '');
    $PMDA->add_indom(SMB2_MOUNT_INDOM, {}, '', '');
    $PMDA->add_indom(ALL_MOUNT_INDOM, {}, '', '');

    $PMDA->set_fetch(\&cifs_fetch);
    $PMDA->set_fetch_callback(\&cifs_fetch_callback);
    $PMDA->set_user('pcp');
}

sub cifs_fetch {
    parse_stat_file();

    my (%smb1, %smb2);

    for my $s (keys %SHARES) {
        if ($SHARES{$s}{proto} eq 'smb1') {
            $smb1{$s} = $SHARES{$s};
        }
        else {
            $smb2{$s} = $SHARES{$s};
        }
    }

    $PMDA->replace_indom(ALL_MOUNT_INDOM, \%SHARES);
    $PMDA->replace_indom(SMB1_MOUNT_INDOM, \%smb1);
    $PMDA->replace_indom(SMB2_MOUNT_INDOM, \%smb2);
    $PMDA->log("fetch: ".Dumper(\%smb1));
}

sub cifs_fetch_callback {
    # TODO: reimpl. all this function

    my ($cluster, $item, $inst) = @_;
    my $pmid = pmda_pmid($cluster, $item);
    my $indom = $PMID_INDOM{$pmid};
    # $PMDA->log("fetch_cb: $cluster, $item, $inst");

    if ($inst != PM_IN_NULL && $indom != PM_INDOM_NULL) {
        my $v = pmda_inst_lookup($indom, $inst);
        $PMDA->log("fetch_cb: ".Dumper($v));
    }

    if ($cluster == CLUSTER_GLOB) {
        if ($inst != PM_IN_NULL) {
            return (PM_ERR_INST, 0);
        }
        if (!defined $RAW[$cluster][$item]) {
            $PMDA->log("UNDEF $cluster, $item");
            return (PM_ERR_PMID, 0);
        }
        return ($RAW[$cluster][$item], 1);
    }
    elsif ($cluster == CLUSTER_MOUNT) {
        if ($inst == PM_IN_NULL) {
            return (PM_ERR_INST, 0);
        }
        if (!defined $RAW[$cluster][$item][$inst]) {
            return (PM_ERR_PMID, 0);
        }
        return ($RAW[$cluster][$item][$inst], 1);
    }
    elsif ($cluster == CLUSTER_TOTAL) {
        if ($inst != PM_IN_NULL) {
            return (PM_ERR_INST, 0);
        }
        if (keys %SHARES == 0) {
            # no shares => each total = 0
            return (0, 1);
        }
        if (!defined $RAW[$cluster][$item]) {
            return (PM_ERR_PMID, 0);
        }
        return ($RAW[$cluster][$item], 1);
    }
    return (PM_ERR_PMID, 0);
}
